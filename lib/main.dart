import 'package:flutter/material.dart';
void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget {
  @override
  _HelloFlutterAppState createState() => _HelloFlutterAppState();
}

String englishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String laosGreeting = "Sabaidee Flutter";
String koreaGreeting = "Unyong Flutter";

class _HelloFlutterAppState extends State<HelloFlutterApp> {
  String displayText = englishGreeting;
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          title: Text("Hello Flutter"),
          leading: Icon(Icons.home),
          actions:<Widget> [
            IconButton(
              onPressed: () {
                setState(() {
                  displayText = displayText == englishGreeting?spanishGreeting:englishGreeting;
                });
    },
              icon: Icon(Icons.refresh)),
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText =  displayText == englishGreeting?laosGreeting:englishGreeting;
                  });
                },
                icon: Icon(Icons.access_time)),
            IconButton(
                onPressed: () {
                  setState(() {
                    displayText =  displayText == englishGreeting?koreaGreeting:englishGreeting;
                  });
                },
                icon: Icon(Icons.account_balance_wallet_outlined))
          ]
        ),
      body: Center (
        child: Text(
          displayText,
          style: TextStyle(fontSize:24),)
        ),
      )
    );
  }
}



// class HelloFlutterApp extends StatelessWidget {
//
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Hello Flutter"),
//           leading: Icon(Icons.home),
//           actions:<Widget> [
//             IconButton(
//               onPressed: () {},
//               icon: Icon(Icons.refresh))
//           ]
//         ),
//       body: Center (
//         child: Text(
//           "Hello Flutter",
//           style: TextStyle(fontSize:24),
//         ),
//       ),
//     );
//   }
//
// }